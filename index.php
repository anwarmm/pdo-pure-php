<?php 
    //memanggil file conn.php yang berisi koneski ke database
    //dengan include, semua kode dalam file conn.php dapat digunakan pada file index.php
    include ('conn.php'); 

    // memulai session
    session_start();
    $status = "";

    //fungsi untuk mencocokkan username dan password
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $user = $_POST['user'];
        $pass = md5($_POST['pass']);

        if(empty($user) || empty($pass)){
            $status = "kosong";
        }else{
            // query PDO
            $sql    = "SELECT username, pass FROM admin WHERE username='$user' AND pass='$pass'";
            $data   = $conn->prepare($sql);
            $data->execute(array($user,$pass));

            if($data->rowCount() >= 1){
                $_SESSION['status'] = 'login';
                header("location: mhs.php");
            }else{
                $status = "salah";
            }
        }
    }    
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Home - Login</title>

  <!-- ===== CSS ===== -->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="assets/css/style.css" />
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato" />
  <!-- ===== CSS ===== -->

</head>
<body>
    <!-- Content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card mx-auto d-block">

                    <div class="card-header bg-primary text-white text-center">
                        <h3 class="display-4">LOGIN</h3>
                    </div>

                    <form action="index.php" method="POST" class="form-signin">

                        <?php 
                            if ($status=="kosong") {
                                echo '<br><br><div class="alert alert-danger" role="alert">Username dan Password harus diisi</div>';
                            }
                            elseif($status=="salah"){
                                echo '<br><br><div class="alert alert-danger" role="alert">Username dan Password tidak ada</div>';
                            }
                        ?>

                        <div class="card-body">
                
                        <div class="form-group">
                            <input type="text" class="form-control" name="user" placeholder="Username">
                        </div>
                
                        <div class="form-group">
                            <input type="password" class="form-control" name="pass" placeholder="Password">
                        </div>
                
                        </div>
                        
                        <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block text-white mx-auto">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="bg bg-primary"></div>
        </div>


    </div>
    <!-- Content -->
        <div class="footer">
            <div class="container">
                <p class="footer-title-left">Tugas Pemrograman Web -1634010025</p>
                <p class="footer-title-right"><span>&copy;</span>&nbsp;2018 Paralel C.</p>
            </div>
        </div>
    

</body>
</html>