<?php 
  //memanggil file conn.php yang berisi koneski ke database
  //dengan include, semua kode dalam file conn.php dapat digunakan pada file index.php
  include ('conn.php'); 
  // memulai session
  session_start();

  // pengecekan session
  if($_SESSION['status'] != 'login'){
    header('Location: index.php');    
  }
  $status = '';
  //melakukan pengecekan apakah ada form yang dipost
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $nip = $_POST['nip'];
      $nama = $_POST['nama'];
      $alamat = $_POST['alamat'];
      
      //query with PDO
      $query = $conn->prepare("INSERT INTO dosen (nip, nama, alamat) VALUES(?, ?, ?)"); 

      //binding data
      $query->bindParam(1,$nip);
      $query->bindParam(2,$nama);
      $query->bindParam(3,$alamat);

      //eksekusi query
      if ($query->execute()) {
        $status = 'berhasil';
      }
      else{
        $status = 'gagal';
      }

     //redirect ke halaman lain
     header('Location: dosen.php?status='.$status);
  }

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Tambah Dosen</title>
    <!-- load css boostrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/dashboard.css" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Pemrograman Web</a>
    </nav>

    <div class="container-fluid">
      <div class="row">
         <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
            <li class="nav-item">
                <h5 class="nav-link">Mahasiswa</h5>
              </li>
               <li class="nav-item">
                <a class="nav-link" href="<?php echo "mhs.php"; ?>">Data Mahasiswa</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo "form_mhs.php"; ?>">Tambah Data</a>
              </li>
              <li class="dropdown-divider"></li>
              <li class="nav-item">
                <h5 class="nav-link">Dosen</h5>
              </li>
               <li class="nav-item">
                <a class="nav-link" href="<?php echo "dosen.php"; ?>">Data Dosen</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="<?php echo "form_dosen.php"; ?>">Tambah Data</a>
              </li>
              <li class="dropdown-divider"></li>
              <li class="nav-item">
                <h5 class="nav-link">Mata Kuliah</h5>
              </li>
               <li class="nav-item">
                <a class="nav-link" href="<?php echo "matkul.php"; ?>">Data Mata Kuliah</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo "form_matkul.php"; ?>">Tambah Data</a>
              </li>
              <li class="dropdown-divider"></li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo "logout.php"; ?>">Logout</a>                
              </li>
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          
          <h2 style="margin: 30px 0 30px 0;">Form Dosen</h2>
          <form action="form_dosen.php" method="POST">
            
            <div class="form-group">
              <label>NIP</label>
              <input type="text" class="form-control" name="nip" required>
            </div>
            <div class="form-group">
              <label>Nama</label>
              <input type="text" class="form-control" name="nama" required>
            </div>
            <div class="form-group">
              <label>Alamat</label>
              <textarea class="form-control" name="alamat" required></textarea>
            </div>
            
            <button type="submit" class="btn btn-primary">Simpan</button>
          </form>
        </main>
      </div>
    </div>

    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.js"></script>
  </body>
</html>