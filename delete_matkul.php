<?php 

  include ('conn.php'); 

  $status = '';
  $result = '';
  //melakukan pengecekan apakah ada form yang dipost
  if ($_SERVER['REQUEST_METHOD'] === 'GET') {
      if (isset($_GET['id'])) {
          //query SQL
          $id_del = $_GET['id'];
          $query = $conn->prepare("DELETE FROM mata_kuliah WHERE id = ? ");
          //binding data
          $query->bindParam(1,$id_del);
          //eksekusi query
          if ($query->execute()) {
            $status = 'delete';
          }
          else{
            $status = 'no_delete';
          }
          //redirect ke halaman lain
          header('Location: matkul.php?status='.$status);
      }  
  }

?>