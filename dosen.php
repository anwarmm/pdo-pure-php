<?php 
  //memanggil file conn.php yang berisi koneski ke database
  //dengan include, semua kode dalam file conn.php dapat digunakan pada file index.php
  include ('conn.php'); 
  // memulai session
  session_start();

  // pengecekan session
  if($_SESSION['status'] != 'login'){
    header('Location: index.php');    
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Dosen</title>
    <!-- load css boostrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/dashboard.css" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Pemrograman Web</a>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <h5 class="nav-link">Mahasiswa</h5>
              </li>
               <li class="nav-item">
                <a class="nav-link" href="<?php echo "mhs.php"; ?>">Data Mahasiswa</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo "form_mhs.php"; ?>">Tambah Data</a>
              </li>
              <li class="dropdown-divider"></li>
              <li class="nav-item">
                <h5 class="nav-link">Dosen</h5>
              </li>
               <li class="nav-item">
                <a class="nav-link active" href="<?php echo "dosen.php"; ?>">Data Dosen</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo "form_dosen.php"; ?>">Tambah Data</a>
              </li>
              <li class="dropdown-divider"></li>
              <li class="nav-item">
                <h5 class="nav-link">Mata Kuliah</h5>
              </li>
               <li class="nav-item">
                <a class="nav-link" href="<?php echo "matkul.php"; ?>">Data Mata Kuliah</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo "form_matkul.php"; ?>">Tambah Data</a>
              </li>
              <li class="dropdown-divider"></li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo "logout.php"; ?>">Logout</a>                
              </li>
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
          <?php 
            //pengecekan status berhasil atau gagal
            if (@$_GET['status']!==NULL) {
              $status = $_GET['status'];
              if ($status=='ok') {
                echo '<br><br><div class="alert alert-success" role="alert">Data Dosen berhasil di-update</div>';
              }
              elseif($status=='err'){
                echo '<br><br><div class="alert alert-danger" role="alert">Data Dosen gagal di-update</div>';
              }
              elseif($status=='delete'){
                echo '<br><br><div class="alert alert-success" role="alert">Data Dosen berhasil di-hapus</div>';
              }
              elseif($status=='no_delete'){
                echo '<br><br><div class="alert alert-danger" role="alert">Data Dosen gagal di-hapus</div>';
              }
              elseif($status=='berhasil'){
                echo '<br><br><div class="alert alert-success" role="alert">Data Dosen berhasil di-tambah</div>';
              }
              elseif($status=='gagal'){
                echo '<br><br><div class="alert alert-danger" role="alert">Data Dosen gagal di-tambah</div>';
              }
            }
           ?>
          <h2 style="margin: 30px 0 30px 0;">Dosen</h2>
          <div class="table-responsive">
            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>NIP</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  //proses menampilkan data dari database dengan PDO:
                  //siapkan query SQL
                  $query = "SELECT * FROM dosen";
                  //eksekusi query
                  $result = $conn->query($query);

                 ?>

                 <?php while($data = $result->fetch(PDO::FETCH_ASSOC) ): ?>
                  <tr>
                    <td><?php echo $data['id'];  ?></td>
                    <td><?php echo $data['nip'];  ?></td>
                    <td><?php echo $data['nama'];  ?></td>
                    <td><?php echo $data['alamat'];  ?></td>
                    <td>
                      <a href="<?php echo "update_dosen.php?id=".$data['id']; ?>" class="btn btn-outline-warning btn-sm"> Update</a>
                      &nbsp;&nbsp;
                      <a href="<?php echo "delete_dosen.php?id=".$data['id']; ?>" class="btn btn-outline-danger btn-sm"> Delete</a>
                    </td>
                  </tr>
                 <?php endwhile ?>
              </tbody>
            </table>
          </div>
        </main>
      </div>
    </div>

    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.js"></script>
  </body>
</html>