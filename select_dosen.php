<?php 

  include ('conn.php'); 

  //proses menampilkan data dari database dengan PDO:
  //siapkan query SQL
  $sql = "SELECT * FROM dosen";
  //eksekusi query
  $query = $conn->query($sql);

  while($data = $query->fetch(PDO::FETCH_ASSOC) ):
?>
    <option value="<?php echo $data['id']; ?>"><?php echo $data['nama']; ?></option>
  <?php endwhile; ?>