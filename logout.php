<?php 

    include ('conn.php'); 
    session_start();
    session_destroy();
    
    //redirect ke halaman lain
    header('Location: index.php');

?>